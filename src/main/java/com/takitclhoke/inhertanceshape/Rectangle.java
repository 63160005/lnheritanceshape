/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhertanceshape;

/**
 *
 * @author ทักช์ติโชค
 */
public class Rectangle extends Shape {

    protected double width;
    protected double height;

    public Rectangle(double width, double height) {
        if (width <= 0 || height <= 0) {
            System.out.println("Error: Redius must more than zero!!!!");
            return;
        }
        this.width = width;
        this.height = height;

    }

    public double recArea() {
        return width * height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWH(double Width, double Height) {
        if (Width <= 0 || Height <= 0) {
            System.out.println("Error: Redius must more than zero!!!!");
            return;
        }
        this.width = width;
        this.height = height;
    }

    @Override
    public void print() {
        System.out.println("Area of rectangle(w = " + this.getWidth() + ",h = " + this.getHeight() + ") is " + this.recArea());
    }

}
