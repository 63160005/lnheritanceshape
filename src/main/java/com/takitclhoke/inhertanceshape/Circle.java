/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhertanceshape;

/**
 *
 * @author ทักช์ติโชค
 */
public class Circle extends Shape {

    private double r;
    private final double pi = 22.0 / 7;

    public Circle(double r) {
        if (r <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.r = r;

    }

    @Override
    public double calArea() {
        return pi * r * r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        if (r <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;
        }
        this.r = r;
    }

    @Override
    public void print() {
        System.out.println("Area of circle(r = " + this.getR() + ") is " + this.calArea());
    }

}
