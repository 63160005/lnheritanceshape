/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhertanceshape;

/**
 *
 * @author ทักช์ติโชค
 */
public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);

    }
    
    public double squArea() {
        return width * height;
    }
    
    @Override
    public void print() {
        System.out.println("Area of squara(w = " + this.getWidth() + ",h = " + this.getHeight() + ") is " + this.recArea());
    }

    
    

}
