/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhertanceshape;

/**
 *
 * @author ทักช์ติโชค
 */
public class Triangle extends Shape {

    private double base;
    private double height;
    public static final double oh = 1.0 / 2;

    public Triangle(double base, double height) {
        if (base <= 0 || height <= 0) {
            System.out.println("Error: Redius must more than zero!!!!");
            return;
        }
        this.base = base;
        this.height = height;

    }

    public double triArea() {
        return oh * base * height;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public void setBH(double base, double height) {
        if (base <= 0 || height <= 0) {
            System.out.println("Error: Redius must more than zero!!!!");
            return;
        }
        this.base = base;
        this.height = height;
    }

    @Override
    public void print() {
        System.out.println("Area of triangle(n = " + this.getBase() + ",b = "
                + this.getHeight() + ") is " + this.triArea());
    }

}
