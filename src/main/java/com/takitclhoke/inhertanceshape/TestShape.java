/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inhertanceshape;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestShape {

    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.print();

        Circle circle = new Circle(0);
        circle.print();

        Triangle triangle = new Triangle(3, 4);
        triangle.print();

        Rectangle rectangle = new Rectangle(3, 4);
        rectangle.print();

        Square square = new Square(2);
        square.print();

        System.out.println("All find area");
        Shape[] shapes = {circle, triangle, rectangle, square};
        for (int i = 0; i < shapes.length; i++) {
            shapes[i].print();
        }

    }

}
